//
//  Utilities.swift
//  AssessmentTest
//
//  Created by Saurabh Sharma on 06/12/19.
//  Copyright © 2019 Saurabh Sharma. All rights reserved.
//

import UIKit

class Utilities {
    
    //MARK:Show Loading
   class func showLoader()  {
           DispatchQueue.main.async {
            appDelegate.window = UIWindow(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
               let loaderView = UIView(frame: (appDelegate.window?.frame)!)
               loaderView.frame = (appDelegate.window?.bounds)!
               loaderView.backgroundColor = .clear
               loaderView.alpha = 0.3
               loaderView.tag = 1001
               let indicatorView = UIActivityIndicatorView(frame: CGRect(x: (loaderView.frame.size.width/2)-40, y: (loaderView.frame.size.height/2)-40, width: 60, height: 60))
               indicatorView.tag = 1002
               indicatorView.color = UIColor.red
               indicatorView.startAnimating()
               loaderView.addSubview(indicatorView)
               indicatorView.startAnimating()
               appDelegate.window?.makeKeyAndVisible()
               if ((appDelegate.window?.viewWithTag(1001)) != nil) {
                   return
               }
               appDelegate.window?.addSubview(loaderView)
           }
       }
      //MARK:Hide Loading
      class func hideLoader()  {
           DispatchQueue.main.async {
               let loaderView = appDelegate.window?.viewWithTag(1001)
               let indicatorView = appDelegate.window?.viewWithTag(1002) as? UIActivityIndicatorView
               indicatorView?.stopAnimating()
               indicatorView?.removeFromSuperview()
               loaderView?.removeFromSuperview()
           }
           
       }
}
