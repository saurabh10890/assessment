//
//  ServiceEndPoints.swift
//  AssessmentTest
//
//  Created by Saurabh Sharma on 06/12/19.
//  Copyright © 2019 Saurabh Sharma. All rights reserved.
//

import UIKit


struct ServiceEndPoints {
    static let apiKey     = "NXXTIa6DJVYOI0hbYZ0RuJgqQFekNUTe"
    static let baseURL    = "https://api.nytimes.com/svc/mostpopular/v2/viewed/"
    static let viewCount = "7"
    static let requestUrl =
        ServiceEndPoints.baseURL
            + "\(ServiceEndPoints.viewCount).json?"
            + "api-key=\(ServiceEndPoints.apiKey)"
    
}




extension Data{
    var pasedJSON: [String :Any]? {
        if let parsedData = try? JSONSerialization.jsonObject(with: self) as? [String: Any] {
            return parsedData
        } else  {
            print("Error  response is : \(String(data: self, encoding: String.Encoding.utf8) ?? "")")
            return nil
        }
    }
}

