//
//  ArticleModel.swift
//  AssessmentTest
//
//  Created by Saurabh Sharma on 06/12/19.
//  Copyright © 2019 Saurabh Sharma. All rights reserved.
//

import UIKit
import Foundation


class ArticleModel :  NSCoding{
    
    var copyright : String!
    var numResults : Int!
    var results : [ArticleResult]!
    var status : String!
    
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        copyright = json["copyright"].stringValue
        numResults = json["num_results"].intValue
        results = [ArticleResult]()
        let resultsArray = json["results"].arrayValue
        for resultsJson in resultsArray{
            let value = ArticleResult(fromJson: resultsJson)
            results.append(value)
        }
        status = json["status"].stringValue
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if copyright != nil{
            dictionary["copyright"] = copyright
        }
        if numResults != nil{
            dictionary["num_results"] = numResults
        }
        if results != nil{
            var dictionaryElements = [[String:Any]]()
            for resultsElement in results {
                dictionaryElements.append(resultsElement.toDictionary())
            }
            dictionary["results"] = dictionaryElements
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }
    
    
    @objc required init(coder aDecoder: NSCoder)
    {
        copyright = aDecoder.decodeObject(forKey: "copyright") as? String
        numResults = aDecoder.decodeObject(forKey: "num_results") as? Int
        results = aDecoder.decodeObject(forKey: "results") as? [ArticleResult]
        status = aDecoder.decodeObject(forKey: "status") as? String
    }
    
    func encode(with aCoder: NSCoder)
    {
        if copyright != nil{
            aCoder.encode(copyright, forKey: "copyright")
        }
        if numResults != nil{
            aCoder.encode(numResults, forKey: "num_results")
        }
        if results != nil{
            aCoder.encode(results, forKey: "results")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        
    }
    
}




class ArticleResult : NSCoding{
    
    var abstractField : String!
    var adxKeywords : String!
    var assetId : Int!
    var byline : String!
    var column : String!
    var desFacet : [String]!
    var geoFacet : String!
    var id : Int!
    var media : [ArticleMedia]!
    var orgFacet : [String]!
    var perFacet : String!
    var publishedDate : String!
    var section : String!
    var source : String!
    var title : String!
    var type : String!
    var uri : String!
    var url : String!
    var views : Int!
    
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        abstractField = json["abstract"].stringValue
        adxKeywords = json["adx_keywords"].stringValue
        assetId = json["asset_id"].intValue
        byline = json["byline"].stringValue
        column = json["column"].stringValue
        desFacet = [String]()
        let desFacetArray = json["des_facet"].arrayValue
        for desFacetJson in desFacetArray{
            desFacet.append(desFacetJson.stringValue)
        }
        geoFacet = json["geo_facet"].stringValue
        id = json["id"].intValue
        media = [ArticleMedia]()
        let mediaArray = json["media"].arrayValue
        for mediaJson in mediaArray{
            let value = ArticleMedia(fromJson: mediaJson)
            media.append(value)
        }
        orgFacet = [String]()
        let orgFacetArray = json["org_facet"].arrayValue
        for orgFacetJson in orgFacetArray{
            orgFacet.append(orgFacetJson.stringValue)
        }
        perFacet = json["per_facet"].stringValue
        publishedDate = json["published_date"].stringValue
        section = json["section"].stringValue
        source = json["source"].stringValue
        title = json["title"].stringValue
        type = json["type"].stringValue
        uri = json["uri"].stringValue
        url = json["url"].stringValue
        views = json["views"].intValue
    }
    
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if abstractField != nil{
            dictionary["abstract"] = abstractField
        }
        if adxKeywords != nil{
            dictionary["adx_keywords"] = adxKeywords
        }
        if assetId != nil{
            dictionary["asset_id"] = assetId
        }
        if byline != nil{
            dictionary["byline"] = byline
        }
        if column != nil{
            dictionary["column"] = column
        }
        if desFacet != nil{
            dictionary["des_facet"] = desFacet
        }
        if geoFacet != nil{
            dictionary["geo_facet"] = geoFacet
        }
        if id != nil{
            dictionary["id"] = id
        }
        if media != nil{
            var dictionaryElements = [[String:Any]]()
            for mediaElement in media {
                dictionaryElements.append(mediaElement.toDictionary())
            }
            dictionary["media"] = dictionaryElements
        }
        if orgFacet != nil{
            dictionary["org_facet"] = orgFacet
        }
        if perFacet != nil{
            dictionary["per_facet"] = perFacet
        }
        if publishedDate != nil{
            dictionary["published_date"] = publishedDate
        }
        if section != nil{
            dictionary["section"] = section
        }
        if source != nil{
            dictionary["source"] = source
        }
        if title != nil{
            dictionary["title"] = title
        }
        if type != nil{
            dictionary["type"] = type
        }
        if uri != nil{
            dictionary["uri"] = uri
        }
        if url != nil{
            dictionary["url"] = url
        }
        if views != nil{
            dictionary["views"] = views
        }
        return dictionary
    }
    
    
    @objc required init(coder aDecoder: NSCoder)
    {
        abstractField = aDecoder.decodeObject(forKey: "abstract") as? String
        adxKeywords = aDecoder.decodeObject(forKey: "adx_keywords") as? String
        assetId = aDecoder.decodeObject(forKey: "asset_id") as? Int
        byline = aDecoder.decodeObject(forKey: "byline") as? String
        column = aDecoder.decodeObject(forKey: "column") as? String
        desFacet = aDecoder.decodeObject(forKey: "des_facet") as? [String]
        geoFacet = aDecoder.decodeObject(forKey: "geo_facet") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        media = aDecoder.decodeObject(forKey: "media") as? [ArticleMedia]
        orgFacet = aDecoder.decodeObject(forKey: "org_facet") as? [String]
        perFacet = aDecoder.decodeObject(forKey: "per_facet") as? String
        publishedDate = aDecoder.decodeObject(forKey: "published_date") as? String
        section = aDecoder.decodeObject(forKey: "section") as? String
        source = aDecoder.decodeObject(forKey: "source") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
        uri = aDecoder.decodeObject(forKey: "uri") as? String
        url = aDecoder.decodeObject(forKey: "url") as? String
        views = aDecoder.decodeObject(forKey: "views") as? Int
    }
    
    
    func encode(with aCoder: NSCoder)
    {
        if abstractField != nil{
            aCoder.encode(abstractField, forKey: "abstract")
        }
        if adxKeywords != nil{
            aCoder.encode(adxKeywords, forKey: "adx_keywords")
        }
        if assetId != nil{
            aCoder.encode(assetId, forKey: "asset_id")
        }
        if byline != nil{
            aCoder.encode(byline, forKey: "byline")
        }
        if column != nil{
            aCoder.encode(column, forKey: "column")
        }
        if desFacet != nil{
            aCoder.encode(desFacet, forKey: "des_facet")
        }
        if geoFacet != nil{
            aCoder.encode(geoFacet, forKey: "geo_facet")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if media != nil{
            aCoder.encode(media, forKey: "media")
        }
        if orgFacet != nil{
            aCoder.encode(orgFacet, forKey: "org_facet")
        }
        if perFacet != nil{
            aCoder.encode(perFacet, forKey: "per_facet")
        }
        if publishedDate != nil{
            aCoder.encode(publishedDate, forKey: "published_date")
        }
        if section != nil{
            aCoder.encode(section, forKey: "section")
        }
        if source != nil{
            aCoder.encode(source, forKey: "source")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }
        if uri != nil{
            aCoder.encode(uri, forKey: "uri")
        }
        if url != nil{
            aCoder.encode(url, forKey: "url")
        }
        if views != nil{
            aCoder.encode(views, forKey: "views")
        }
        
    }
    
}


class ArticleMedia :  NSCoding{
    
    var approvedForSyndication : Int!
    var caption : String!
    var copyright : String!
    var mediaMetadata : [ArticleMediaMetadatum]!
    var subtype : String!
    var type : String!
    
    
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        approvedForSyndication = json["approved_for_syndication"].intValue
        caption = json["caption"].stringValue
        copyright = json["copyright"].stringValue
        mediaMetadata = [ArticleMediaMetadatum]()
        let mediaMetadataArray = json["media-metadata"].arrayValue
        for mediaMetadataJson in mediaMetadataArray{
            let value = ArticleMediaMetadatum(fromJson: mediaMetadataJson)
            mediaMetadata.append(value)
        }
        subtype = json["subtype"].stringValue
        type = json["type"].stringValue
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if approvedForSyndication != nil{
            dictionary["approved_for_syndication"] = approvedForSyndication
        }
        if caption != nil{
            dictionary["caption"] = caption
        }
        if copyright != nil{
            dictionary["copyright"] = copyright
        }
        if mediaMetadata != nil{
            var dictionaryElements = [[String:Any]]()
            for mediaMetadataElement in mediaMetadata {
                dictionaryElements.append(mediaMetadataElement.toDictionary())
            }
            dictionary["mediaMetadata"] = dictionaryElements
        }
        if subtype != nil{
            dictionary["subtype"] = subtype
        }
        if type != nil{
            dictionary["type"] = type
        }
        return dictionary
    }
    
    @objc required init(coder aDecoder: NSCoder)
    {
        approvedForSyndication = aDecoder.decodeObject(forKey: "approved_for_syndication") as? Int
        caption = aDecoder.decodeObject(forKey: "caption") as? String
        copyright = aDecoder.decodeObject(forKey: "copyright") as? String
        mediaMetadata = aDecoder.decodeObject(forKey: "media-metadata") as? [ArticleMediaMetadatum]
        subtype = aDecoder.decodeObject(forKey: "subtype") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
    }
    
    func encode(with aCoder: NSCoder)
    {
        if approvedForSyndication != nil{
            aCoder.encode(approvedForSyndication, forKey: "approved_for_syndication")
        }
        if caption != nil{
            aCoder.encode(caption, forKey: "caption")
        }
        if copyright != nil{
            aCoder.encode(copyright, forKey: "copyright")
        }
        if mediaMetadata != nil{
            aCoder.encode(mediaMetadata, forKey: "media-metadata")
        }
        if subtype != nil{
            aCoder.encode(subtype, forKey: "subtype")
        }
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }
        
    }
    
}



class ArticleMediaMetadatum :  NSCoding{
    
    var format : String!
    var height : Int!
    var url : String!
    var width : Int!
    
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        format = json["format"].stringValue
        height = json["height"].intValue
        url = json["url"].stringValue
        width = json["width"].intValue
    }
    
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if format != nil{
            dictionary["format"] = format
        }
        if height != nil{
            dictionary["height"] = height
        }
        if url != nil{
            dictionary["url"] = url
        }
        if width != nil{
            dictionary["width"] = width
        }
        return dictionary
    }
    
    
    @objc required init(coder aDecoder: NSCoder)
    {
        format = aDecoder.decodeObject(forKey: "format") as? String
        height = aDecoder.decodeObject(forKey: "height") as? Int
        url = aDecoder.decodeObject(forKey: "url") as? String
        width = aDecoder.decodeObject(forKey: "width") as? Int
    }
    
    func encode(with aCoder: NSCoder)
    {
        if format != nil{
            aCoder.encode(format, forKey: "format")
        }
        if height != nil{
            aCoder.encode(height, forKey: "height")
        }
        if url != nil{
            aCoder.encode(url, forKey: "url")
        }
        if width != nil{
            aCoder.encode(width, forKey: "width")
        }
        
    }
    
}
