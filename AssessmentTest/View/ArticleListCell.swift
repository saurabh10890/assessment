//
//  ArticleListCell.swift
//  AssessmentTest
//
//  Created by Saurabh Sharma on 06/12/19.
//  Copyright © 2019 Saurabh Sharma. All rights reserved.
//

import UIKit

class ArticleListCell: UITableViewCell {
    
    //MARK:-IBOutlet & Properties.
    //MARK:
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var headingTextLabel: UILabel!
    @IBOutlet weak var subTitleTextLabel: UILabel!
    @IBOutlet weak var dateTextLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleImageView.layer.cornerRadius = self.titleImageView.frame.height/2
        self.titleImageView.clipsToBounds = true
        self.accessoryType = .disclosureIndicator
    }
    
    //MARK:-setData
    //MARK:
    func setData(_ model:ArticleResult?,indexPath:IndexPath){
        self.headingTextLabel.text = "\(model?.title ?? "")"
        self.subTitleTextLabel.text = "\(model?.byline ?? "")"
        self.authorNameLabel.text = "\(model?.source ?? "")"
        self.dateTextLabel.text = "\(model?.publishedDate ?? "")"


        
    }
}

