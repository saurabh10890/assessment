//
//  ArticleDetailVC.swift
//  AssessmentTest
//
//  Created by Saurabh Sharma on 06/12/19.
//  Copyright © 2019 Saurabh Sharma. All rights reserved.
//

import UIKit

class ArticleDetailVC: UIViewController {
    
    //MARK:-IBOutlet & Properties.
    //MARK:
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var headingTextLabel: UILabel!
    @IBOutlet weak var subTitleTextLabel: UILabel!
    @IBOutlet weak var dateTextLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    var model:ArticleResult?
    var icon:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleImageView.layer.cornerRadius = self.titleImageView.frame.height/2
        self.titleImageView.clipsToBounds = true
        self.setData(model)
        // Do any additional setup after loading the view.
    }
    
    //MARK:-setData
    //MARK:
    func setData(_ model:ArticleResult?){
        self.headingTextLabel.text = "\(model?.title ?? "")"
        self.subTitleTextLabel.text = "\(model?.abstractField ?? "")"
        self.authorNameLabel.text = "\(model?.adxKeywords ?? "")"
       // self.dateTextLabel.text = "\(model?.publishedDate ?? "")"
    }
}

