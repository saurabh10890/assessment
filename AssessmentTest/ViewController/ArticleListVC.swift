//
//  ArticleListVC.swift
//  AssessmentTest
//
//  Created by Saurabh Sharma on 06/12/19.
//  Copyright © 2019 Saurabh Sharma. All rights reserved.
//

import UIKit

class ArticleListVC: UIViewController {
    
    //MARK:-IBOutlet & Properties.
    //MARK:
    @IBOutlet weak var articleTableView: UITableView!
    lazy var articleViewModel =  ArticleViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configuration()
        // Do any additional setup after loading the view.
    }
    
    //MARK:-Functions.
    //MARK:
    private func configuration(){
        self.articleTableView.delegate = self
        self.articleTableView.dataSource = self
        self.articleTableView.tableFooterView = UIView()
        self.articleTableView.register(UINib(nibName: "EmptyDataCell", bundle: nil), forCellReuseIdentifier: "EmptyDataCell")
        
        self.articleViewModel.gettingArticleModeling({ (model) in
            DispatchQueue.main.async {
                self.articleTableView.reloadData()
            }
        })
    }
}

//MARK:- UITableViewDelegate & Datasource
//MARK:
extension ArticleListVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articleViewModel.model?.results?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = self.articleViewModel.model?.results?.count ?? 0
        
        guard model > 0  else {
            let emptyCell = tableView.dequeueReusableCell(withIdentifier: "EmptyDataCell") as! EmptyDataCell
            return emptyCell
        }
        let articleCell = tableView.dequeueReusableCell(withIdentifier: "ArticleListCell") as! ArticleListCell
        articleCell.setData(self.articleViewModel.model?.results?[indexPath.row], indexPath: indexPath)
        return articleCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let model = self.articleViewModel.model?.results?.count ?? 0
        guard model > 0  else {
            return tableView.frame.height
        }
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.articleViewModel.model?.results?.count ?? 0
        guard model > 0  else {
            return
        }
        tableView.deselectRow(at: indexPath, animated: true)
        let articleDetailVC =
            self.storyboard?.instantiateViewController(identifier: "ArticleDetailVC") as! ArticleDetailVC
        articleDetailVC.model = self.articleViewModel.model?.results?[indexPath.row]
        let cell = tableView.cellForRow(at: indexPath) as! ArticleListCell
        articleDetailVC.icon = cell.titleImageView.image
        self.navigationController?.pushViewController(articleDetailVC, animated: true)
    }
}
