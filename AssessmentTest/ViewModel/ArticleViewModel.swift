//
//  ArticleViewModel.swift
//  AssessmentTest
//
//  Created by Saurabh Sharma on 06/12/19.
//  Copyright © 2019 Saurabh Sharma. All rights reserved.
//

import UIKit


enum VMCustomError:Error{
    case InvalidJson
    case NotFount
    
}

class ArticleViewModel {
    
    typealias callback = (_ model:ArticleModel?) ->()
    var model:ArticleModel?
    
    init() {
    }
    
    //MARK:
    func gettingArticleModeling(_ callbackHandler:callback?){
        Utilities.showLoader()
        printDebug(ServiceEndPoints.requestUrl)
        ServiceManager.manager.weatherService(ServiceEndPoints.requestUrl, param: nil, success: { (data) in
            
            printDebug(data?.pasedJSON)
            Utilities.hideLoader()
            let model = self.responseParsing(JSON(data?.pasedJSON ?? [:]))
            callbackHandler?( model)
        }) { (error) in
            Utilities.hideLoader()
            printDebug(error)
        }
    }
    
    func responseParsing(_ response:JSON?)->ArticleModel?{
        if let res = response{
            self.model = ArticleModel(fromJson: res)
        }
        return self.model
        
    }
}


