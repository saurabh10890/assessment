//
//  ServiceManager.swift
//  AssessmentTest
//
//  Created by Saurabh Sharma on 06/12/19.
//  Copyright © 2019 Saurabh Sharma. All rights reserved.
//
import UIKit

class ServiceManager {
    
    static let manager = ServiceManager()
    
    //MARK:-Configuration
    //Mark:-
    var session:URLSession?
    var sessionTask:URLSessionTask?
    
    //MARK:-Init
    //Mark:-
    private init(){
        self.session = URLSession(configuration:URLSessionConfiguration.default)
        self.sessionTask = URLSessionTask()
    }
    
    //MARK:-Block Creation
    //Mark:-
    typealias Success = (_ response: Data?) ->()
    typealias Failure = (_ error: Error?) ->()
    
    //MARK: API Calling
    //MARK:
    func weatherService(_ url:String,param:[String:Any]?,success: Success?,failure:Failure?){
        if let requestUrl = URL(string:url){
            let request = createRequest(url:requestUrl)
            // create body
            self.startSession(withRequest: request, success: { (data) in
                success?(data)
            }) { (error) in
                failure?(error)
            }
        } else {
            failure?(nil)
        }
    }
    
    //MARK: startSession
    //MARK:
    private func startSession(withRequest request: URLRequest,success: Success?,failure:Failure?) {
        sessionTask = session!.dataTask(with: request, completionHandler: { (data:Data?, response, error) in
            
            if let nsdata = data {
                success?(nsdata)
            }
            if let err = error {
                failure?(err)
            }
        })
        self.sessionTask!.resume()
    }
    
    //MARK: createRequest
    //MARK:
    private func createRequest(url:URL) -> URLRequest {
        var request = URLRequest(url:url, cachePolicy:.useProtocolCachePolicy, timeoutInterval:60)
        request.httpMethod = "get"
        let headers =  requestHeader
        request.allHTTPHeaderFields = headers
        return request
    }
    
    //MARK: requestHeader
    //MARK:
    private var requestHeader: [String:String] {
        return ["content-type":"application/json"]
    }
    
}

